<?php

include_once 'connect/connection.php';
include_once 'include/navigation.php';
include_once 'include/function.php';
$q = 'select count(contactID) from contact';
$rq = mysqli_fetch_array(mysqli_query($link, $q));
$sum = $rq[0];
$numItems = 5;
$numPages = $sum / $numItems;

$page = 1;
if (isset($_REQUEST['page']))
    $page=$_REQUEST['page'];
$start = ($page - 1)*$numItems;
?>
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Contact Information</h1>
    </div>
</div>
<?php


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete-contact'])) {
    $delete_id = $_POST['delete-contact'];
    $query_delete = "DELETE FROM `contact` WHERE contactID=$delete_id";
    $result = mysqli_query($link, $query_delete);
    confirm_query($result, $query_delete);
}

?>

<div class="row">
    <div class="col-lg-12">

        <?php

        if (isset($_POST['delete-contact'])) {
            if (mysqli_affected_rows($link) == 1) {
                echo "<div class=\"alert alert-success\" id=\"div-action-success\">Delete Successfully</div>";
            }
        }
        ?>


        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Category</div>
            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    $sort = 'contactDate';
                    if (isset($_GET['sort'])) {
                        $num = $_GET['sort'];
                        switch ($num) {
                            case 0:
                                $sort = 'contactName';
                                break;
                            case 1:
                                $sort = 'contactEmail';
                                break;
                            case 2:
                                $sort = 'contactMessage';
                                break;
                            default:
                                $sort = 'contactDate';
                                break;
                        }
                    }

                    ?>
                    <table class="table table-bordered table-striped table-hover" id="contact-table">
                        <thead>


                        <td class="col-md-2">
                            <a href="contact.php?sort=0&contact=1">Contact
                                Name<?php if ($sort == 'contactName') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                        </td>
                        <td class="col-md-2">
                            <a href="contact.php?sort=1&contact=1">Email
                                <?php if ($sort == 'contactEmail') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                        </td>
                        <td class="col-md-5">
                            <a href="contact.php?sort=2&contact=1">Message <?php if ($sort == 'contactMessage') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                        </td>
                        <td class="col-md-2">
                            <a href="contact.php?sort=3&contact=1">Date<?php if ($sort == 'contactDate') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                        </td>


                        <td class="col-md-1"></td>

                        </thead>
                        <?php

                        $query = "SELECT `contactID`, `contactName`, `contactEmail`, `contactMessage`,contactDate ";
                        $query .= " FROM contact ";
                        $query .= " ORDER BY $sort ASC ";
                        $query .= " limit $start,$numItems "; //pagination

                        $result = mysqli_query($link, $query);
                        confirm_query($result, $query);

                        while ($row = mysqli_fetch_array($result)) {
                            echo "<tr>

                                <td>{$row[1]}</td>
                                <td>{$row[2]}</td>
                                <td class='div-show-message'>{$row[3]}</td>
                                <td>{$row[4]}</td>


                                <td>
                                    <button class='btn btn-danger' id='btn-toggle-delete-" . $row[0] . "' type='button' data-toggle='collapse' data-target='#form-delete-category-" . $row[0] . "' >Delete</button>
                                    <div class='collapse' id='form-delete-category-" . $row[0] . "'>

                                    <form  method='post' action='contact.php?contact=1'>
                                        <div class='form-group'>
                                            <label for='delete-category'>Are You Sure</label>

                                                <button type='submit' name='delete-contact' value='$row[0]'  class='btn btn-danger btn-block '>Yes</a>
                                                <button class='btn btn-success btn-block' type='button' data-toggle='collapse' data-target='#form-delete-category-" . $row[0] . "' >No</button>

                                        </div>
                                    </form>


                                </td>
                            </tr>";
                        }


                        ?>

                    </table>
                </div>
            </div>

            <div class="panel-footer">
                <nav>
                    <ul class="pagination">
                        <?php

                        $page = $_REQUEST['page'];
                        if (!isset($page)) {
                            $page = 1;
                        }
                        if ($numPages > 1) {
                            if ($page != 1) {
                                if ($page > 1) {
                                    $previousPage = $page - 1;
                                    echo
                                    "
                                        <li>
                                            <a href='contact.php?contact=1&page=$previousPage aria-label='Previous'>
                                                <span>&laquo;</span >
                                            </a >
                                        </li >

                                    ";
                                }
                            }
                            for ($i = 1; $i < $numPages + 1; $i++) {
                                $selected = '';
                                if ($page == $i) {
                                    $selected = 'selected';
                                }
                                echo
                                "
                                        <li ><a class='$selected'  href='contact.php?contact=1&page=$i'>$i</a></li>
                                    ";
                            }

                            if ($page < $numPages) {
                                $nextPage = $page + 1;

                                echo
                                "
                                        <li>
                                            <a href='contact.php?contact=1&page=$nextPage' aria-label='Previous'>
                                                <span>&raquo;</span >
                                            </a >
                                        </li >

                                    ";
                            }

                        }

                        ?>


                    </ul>
                </nav>
            </div>
        </div>

    </div>
</div>
<!--/.table-->


<script>
    setTimeout(function () {
        // Do something after 3 seconds
        $('#div-action-success').fadeOut();
    }, 3000);
    var messages = $('.div-show-message');

    for(i=0;i<messages.length;i++) {
        var popover='<a tabindex="0"  role="button" data-toggle="popover" data-trigger="focus" title="Full Message" data-content="'+messages[i].innerHTML+'">Read more</a>'
        messages[i].innerHTML = messages[i].innerHTML.substring(0,100)+'....' + popover;

    }
    $(function () {
        $('[data-toggle="popover"]').popover()

    })

</script>
<?php include_once 'include/footer.php'; ?>