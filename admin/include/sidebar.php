<div class="navbar-default sidebar" role="navigation">
    <div class="side-nav navbar-collapse">
        <ul class="nav " id="side-menu">
            <li>
                <a href="contact.php" ><i class="glyphicon glyphicon-dashboard"></i>Dashboard</a>
            </li>
            <li class="<?php if (isset($_GET['category']) && $_GET['category']==1) {echo 'selected';}?>">
                <a href="category.php?category=1"><i class="glyphicon glyphicon-folder-close"></i>Category</a>
            </li>
            <li>
                <a href="#product-collapse-second-nav" data-toggle="collapse"><i
                        class="glyphicon glyphicon-phone"></i>Products<span
                        class="glyphicon glyphicon-menu-down pull-right"></span></a>
                <ul class="nav navbar-second-level collapse" id="product-collapse-second-nav">
                    <li>
                        <a href="contact.php"><i class="glyphicon glyphicon-tag"></i>Product</a>
                    </li>
                    <li>
                        <a href="contact.php"><i class="glyphicon glyphicon-tag"></i>Product Type</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#services-collapse-second-nav" data-toggle="collapse"><i
                        class="glyphicon glyphicon-share"></i>Services<span
                        class="glyphicon glyphicon-menu-down pull-right"></span></a>

                <ul class="nav navbar-second-level collapse" id="services-collapse-second-nav">
                    <li>
                        <a href="contact.php"><i class="glyphicon glyphicon-tag"></i>Service</a>
                    </li>
                    <li>
                        <a href="contact.php"><i class="glyphicon glyphicon-tag"></i>Service Type</a>
                    </li>
                </ul>

            </li>

            <li>
                <a href="#album-collapse-second-nav" data-toggle="collapse"><i
                        class="glyphicon glyphicon-camera"></i>Albums & Photos<span
                        class="glyphicon glyphicon-menu-down pull-right"></span></a>
                <ul class="nav navbar-second-level collapse" id="album-collapse-second-nav">
                    <li>
                        <a href="contact.php"><i class="glyphicon glyphicon-book"></i>Album</a>
                    </li>
                    <li>
                        <a href="contact.php"><i class="glyphicon glyphicon-flash"></i>Photo</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="contact.php"><i class="glyphicon glyphicon-hd-video"></i>Video</a>
            </li>
            <li>
                <a href="contact.php"><i class="glyphicon glyphicon-certificate"></i>News</a>
            </li>
            <li class="<?php if (isset($_GET['contact']) && $_GET['contact']==1) {echo 'selected';}?>">
                <a href="contact.php?contact=1"><i class="glyphicon glyphicon-phone-alt"></i>Contact</a>
            </li>
        </ul>
    </div>
</div>