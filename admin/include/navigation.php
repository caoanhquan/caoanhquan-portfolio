<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lensseen admin</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/admin.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
</head>
<body>


<nav class="navbar navbar-default" id="top-nav-bar">

    <!--toggle button for mobile display-->
    <div class="navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#lensseen-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="#">LENSEEN ADMIN</a>
    </div>
    <!-- end navbar-header>

    <!-- collect the nav content for toggling-->
    <div class="collapse navbar-collapse" id="lensseen-navbar-collapse">
        <ul class="nav navbar-nav navbar-right navbar-top-link">
            <li class="dropdown">
                <a class="dropdown-toggle " data-toggle="dropdown" href="#">
                    Welcome,<strong> Admin</strong> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                    <li><a tabindex="-1" href="#"><i class="glyphicon glyphicon-user "></i> <span
                                class="navbar-top-link-text">User Profile</span> </a></li>
                    <li><a tabindex="-1" href="#"><i class="glyphicon glyphicon-edit "></i> <span
                                class="navbar-top-link-text">Setting</span> </a></li>
                </ul>
            </li>
        </ul>
        <!-- end navbar-top-link-->

        <?php include_once 'sidebar.php' ?>
        <!--end sidebar-->


    </div>
    <!-- end nav -->
</nav>
