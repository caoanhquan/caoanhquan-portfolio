<?php
define('BASE_URL', 'http:localhost/lensseen/');
define('LIVE', false);//false la dang trong qua trinh phat trien true la dang hoat dong roi
function confirm_query($result, $query)
{
    global $link;
    if (!$result && !LIVE) {
        die("Query {$query} \n<br/> MySQL Error: " . mysqli_error($link));
    }
}

function cat_pos($num)
{
    switch ($num) {
        case 0:
            return 'home-slider';
        case 1:
            return 'work';
        case 2:
            return 'about';
        case 3:
            return 'contact';

    }
}

?>