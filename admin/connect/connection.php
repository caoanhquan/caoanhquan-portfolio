<?php
/**
 * Created by PhpStorm.
 * User: Cao Anh Quan
 * Date: 3/6/2015
 * Time: 8:54 AM
 */
error_reporting(E_ERROR | E_PARSE);
//connect to database
$link = mysqli_connect('localhost', 'root', '', 'lensseen');

//Check if connection is false,
if (!$link) {
    trigger_error('Could not connect to DB: ' . mysqli_connect_error());
} else {
    mysqli_set_charset($link,'utf-8');
}
?>