<?php

include_once 'connect/connection.php';
include_once 'include/navigation.php';
include_once 'include/function.php';
$q = 'select count(categoryID) from category';
$rq = mysqli_fetch_array(mysqli_query($link, $q));
$sum = $rq[0];
$numItems = 5;
$numPages = $sum / $numItems;
?>
    <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Category</h1>
        </div>
    </div>
    <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) { //Gia tri ton tai xu ly form
        $error_message = '';
        $errors = array();
        if (empty($_POST['input-add-category-name'])) {
            $errors[] = 'category';
            $error_message .= "You must fill in the category";
        } else {
            $cat_name = mysqli_real_escape_string($link, strip_tags($_POST['input-add-category-name']));
        }
        if (isset($_POST['select-add-category-parent'])) {
            $cat_parent_id = $_POST['select-add-category-parent'];
        } else {
            $errors[] = 'parent';
            $error_message .= "<br/>You must select the parent category";
        }
        if (isset($_POST['input-add-category-position'])) {
            $cat_position = $_POST['input-add-category-position'];
        } else {
            $errors[] = 'position';
            $error_message .= "<br/>You must fill in the Category Position";
        }

        if (empty($errors)) {
            $query = "insert into category(categoryName,categoryParentID,categoryPosition) values('{$cat_name}',{$cat_parent_id},{$cat_position})";
            $result = mysqli_query($link, $query) or die("Query {$query} <br/> MYSQL Error: " . mysqli_error($link));
        } else {
            echo "<div class=\"alert alert-danger\">$error_message</div>";
        }
    }
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['edit-submit'])) { //Gia tri ton tai xu ly form
        $error_message = '';
        $errors = array();
        $cat_id = $_POST['input-edit-category-id'];
        if (empty($_POST['input-edit-category-name'])) {
            $errors[] = 'category';
            $error_message .= "You must fill in the category";
        } else {
            $cat_name = mysqli_real_escape_string($link, strip_tags($_POST['input-edit-category-name']));
        }
        if (isset($_POST['select-edit-category-parent'])) {
            $cat_parent_id = $_POST['select-edit-category-parent'];
        } else {
            $errors[] = 'parent';
            $error_message .= "<br/>You must select the parent category";
        }
        if (isset($_POST['input-edit-category-position'])) {
            $cat_position = $_POST['input-edit-category-position'];
        } else {
            $errors[] = 'position';
            $error_message .= "<br/>You must fill in the Category Position";
        }

        if (empty($errors)) {
            $query = "UPDATE `category` SET `categoryName`='$cat_name',`categoryParentID`=$cat_parent_id,`categoryPosition`=$cat_position WHERE categoryID=$cat_id";
            $result = mysqli_query($link, $query) or die("Query {$query} <br/> MYSQL Error: " . mysqli_error($link));
        } else {
            echo "<div class=\"alert alert-danger\">$error_message</div>";
        }
    }
    if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['delete-category'])) {
        $delete_id = $_POST['delete-category'];
        $query_delete = "DELETE FROM `category` WHERE categoryID=$delete_id";
        $result = mysqli_query($link, $query_delete);
        confirm_query($result, $query_delete);
    }

    ?>

    <div class="row">
    <div class="col-lg-12">
    <div style="margin-bottom: 20px">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add-category-modal">Add
            New Category
        </button>

    </div>
    <?php

    if (isset($_POST['submit'])) {
        if (mysqli_affected_rows($link) == 1) {
            echo "<div class=\"alert alert-success\" id=\"div-add-success\">Add Successfully</div>";
        }
    }
    if (isset($_POST['edit-submit'])) {
        if (mysqli_affected_rows($link) == 1) {
            echo "<div class=\"alert alert-success\" id=\"div-add-success\">Edit Successfully</div>";
        }
    }
    if (isset($_POST['delete-category'])) {
        if (mysqli_affected_rows($link) == 1) {
            echo "<div class=\"alert alert-success\" id=\"div-add-success\">Delete Successfully</div>";
        }
    }
    ?>


    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Category</div>
        </div>

        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <?php
                $sort = 'categoryPosition';
                if (isset($_GET['sort'])) {
                    $num = $_GET['sort'];
                    switch ($num) {
                        case 0:
                            $sort = 'categoryID';
                            break;
                        case 1:
                            $sort = 'categoryName';
                            break;
                        case 2:
                            $sort = 'categoryParentID';
                            break;
                        case 3:
                            $sort = 'categoryPosition';
                            break;
                        case 4:
                            $sort = 'adminName';
                            break;
                        case 5:
                            $sort = 'adminID';
                            break;
                        default:
                            $sort = 'categoryPosition';
                            break;
                    }
                }

                ?>
                <table class="table table-bordered table-striped table-hover" id="contact-table">
                    <thead>


                    <td class="col-md-3"><a href="category.php?sort=1&category=1">Category
                            Name<?php if ($sort == 'categoryName') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                    </td>
                    <td class="col-md-2"><a href="category.php?sort=2&category=1">Category Parent
                            <?php if ($sort == 'categoryParentID') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                    </td>
                    <td class="col-md-2"><a href="category.php?sort=3&category=1">Category
                            Position<?php if ($sort == 'categoryPosition') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                    </td>
                    <td class="col-md-3"><a href="category.php?sort=4&category=1">Added
                            By<?php if ($sort == 'adminName') echo "<i class='glyphicon glyphicon-triangle-bottom pull-right'></i>"; ?></a>
                    </td>

                    <td class="col-md-1"></td>
                    <td class="col-md-1"></td>
                    </thead>
                    <?php
                    $page = 1;
                    if (isset($_REQUEST['page']))
                        $page=$_REQUEST['page'];
                    $start = ($page - 1)*$numItems;

                    $query = " SELECT `categoryID`, `categoryName`, `categoryParentID`, `categoryPosition`,`adminName`,`adminID` ";
                    $query .= " FROM category ";
                    $query .= " JOIN admin ";
                    $query .= " USING(adminID)  ";
                    $query .= " ORDER BY $sort ASC ";
                    $query .= " limit $start,$numItems ";

                    $result = mysqli_query($link, $query);
                    confirm_query($result, $query);
                    while ($row = mysqli_fetch_array($result)) {
                        $parent = 'none';
                        $query1 = "SELECT `categoryID`, `categoryName`, `categoryParentID`, `categoryPosition`,`adminName`,`adminID`, ";
                        $query1 .= " FROM category ";
                        $result1 = mysqli_query($link, $query);
                        confirm_query($result1, $query1);
                        while ($subrow = mysqli_fetch_array($result1)) {
                            if ($subrow[0] == $row[2]) {
                                $parent = $subrow[1];
                                break;
                            }
                        }
                        echo "<tr>

                                <td>{$row[1]}</td>
                                <td>$parent</td>
                                <td>{$row[3]}</td>
                                <td>{$row[4]}</td>

                                <td>
                                        <button type='button' id='edit-button' class='btn btn-success'
                                        data-catid='{$row[0]}'
                                        data-catname='{$row[1]}'
                                        data-catparent='{$row[2]}'
                                        data-catposition='{$row[3]}'
                                        data-target='#edit-category-modal' data-toggle='modal'>Edit</button>

                                </td>
                                <td>
                                    <button class='btn btn-danger' id='btn-toggle-delete-" . $row[0] . "' type='button' data-toggle='collapse' data-target='#form-delete-category-" . $row[0] . "' >Delete</button>
                                    <div class='collapse' id='form-delete-category-" . $row[0] . "'>

                                    <form  method='post' action='category.php?category=1'>
                                        <div class='form-group'>
                                            <label for='delete-category'>Are You Sure</label>

                                                <button type='submit' name='delete-category' value='$row[0]'  class='btn btn-danger btn-block '>Yes</a>
                                                <button class='btn btn-success btn-block' type='button' data-toggle='collapse' data-target='#form-delete-category-" . $row[0] . "' >No</button>

                                        </div>
                                    </form>


                                </td>
                            </tr>";
                    }


                    ?>


                </table>
            </div>
        </div>
        <div class="panel-footer">
            <nav>
                <ul class="pagination">
                    <?php

                    $page = $_REQUEST['page'];
                    if (!isset($page)) {
                        $page = 1;
                    }
                    if ($numPages > 1) {
                        if ($page != 1) {
                            if ($page > 1) {
                                $previousPage = $page - 1;
                                echo
                                "
                                        <li>
                                            <a href='category.php?category=1&page=$previousPage aria-label='Previous'>
                                                <span>&laquo;</span >
                                            </a >
                                        </li >

                                    ";
                            }
                        }
                        for ($i = 1; $i < $numPages + 1; $i++) {
                            $selected = '';
                            if ($page == $i) {
                                $selected = 'selected';
                            }
                            echo
                            "
                                        <li ><a class='$selected'  href='category.php?category=1&page=$i'>$i</a></li>
                                    ";
                        }

                        if ($page < $numPages) {
                            $nextPage = $page + 1;

                            echo
                            "
                                        <li>
                                            <a href='category.php?category=1&page=$nextPage' aria-label='Previous'>
                                                <span>&raquo;</span >
                                            </a >
                                        </li >

                                    ";
                        }

                    }

                    ?>


                </ul>
            </nav>
        </div>
    </div>

    </div>

    </div>
    <!--/.table-->

    <!--Begin edit category modal-->
    <div class="modal fade" id="edit-category-modal">
        <div class="modal-dialog">

            <form method="post" name="form-edit-category" id="form-edit-category" onsubmit="return validateEditForm()"
                  action="category.php?category=1">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data
                        - dismiss = "modal" ><span>&times;</span></button >
                        <h4 class="modal-title" id="edit-modal-title"> Edit Category </h4>
                    </div>

                    <div class="modal-body">

                        <!--Begin input category id-->
                        <div class="form-group" id="group-edit-category-id">
                            <label for="input-edit-category-id"> Category ID </label>
                            <input autocomplete type="text" tabindex="1" class="form-control"
                                   name="input-edit-category-id" readonly
                                   id="input-edit-category-id" value=""
                                >
                        </div>


                        <!--End input category id-->

                        <!--Begin input category name-->
                        <div class="form-group " id="group-edit-category-name">
                            <label for="input-edit-category-name"> Category Name </label>
                            <input autocomplete type="text" tabindex="1" class="form-control"
                                   name="input-edit-category-name"
                                   id="input-edit-category-name" value=""
                                   placeholder="Enter Name">
                        </div>
                        <div class="alert alert-danger display-error" id="edit-category-name-error"> Please Fill in
                            Category Name
                        </div>
                        <!--End input category name-->

                        <!--Begin input category position-->
                        <div class="form-group " id="group-edit-category-position">
                            <label for="input-edit-category-position"> Position</label>
                            <input autocomplete type="number" tabindex="1" class="form-control"
                                   name="input-edit-category-position"
                                   id="input-edit-category-position" value=""
                                >
                        </div>
                        <div class="alert alert-danger display-error" id="edit-category-position-error"> Please Fill in
                            Category Position
                        </div>
                        <!--End input category position-->

                        <!--Begin select category parent ID-->
                        <div class="form-group has-error " id="group-edit-category-parent">
                            <label for="select-edit-category-parent"> Select Category Parent </label>
                            <select name="select-edit-category-parent" id="select-edit-category-parent">
                                class="form-control
                                " >
                                <option value="0"> None</option>
                                <?php
                                $query = "SELECT `categoryID`, `categoryName`, `categoryParentID` FROM `category` WHERE 1 ";
                                $result = mysqli_query($link, $query) or die(mysqli_connect_error());
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=\"$row[0]\">$row[1]</option>";
                                }


                                ?>

                            </select>
                        </div>
                        <div class="alert alert-danger display-error" id="edit-category-parent-error">Please Select The
                            Category Parent ID
                        </div>
                        <!-- End select category parent ID -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" style="margin-top:8px" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" id="edit-submit" name="edit-submit" value="edit-submit"
                                class="btn btn-success"
                                style="margin-top: 10px">Save
                        </button>
                    </div>
            </form>
        </div>

    </div>
    <!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Begin edit category modal -->

    <!-- Begin add category modal -->
    <div class="modal fade" id="add-category-modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <form method="post" name="form-add-category" id="form_add_category" onsubmit="return validateForm()"
                  action="category.php?category=1">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        <h4 class="modal-title">Add New Category</h4>
                    </div>

                    <div class="modal-body">

                        <!-- Begin input category name -->
                        <div class="form-group " id="group-add-category-name">
                            <label for="input-name">Category Name</label>
                            <input autocomplete type="text" tabindex="1" class="form-control"
                                   name="input-add-category-name"
                                   id="input-add-category-name" value=""
                                   placeholder="Enter Name">
                        </div>
                        <div class="alert alert-danger display-error" id="add-category-name-error">Please Fill in
                            Category Name
                        </div>
                        <!-- End input category name -->

                        <!-- Begin input category position -->
                        <div class="form-group " id="group-add-category-position">
                            <label for="input-name">Position</label>
                            <input autocomplete type="number" tabindex="1" class="form-control"
                                   name="input-add-category-position"
                                   id="input-add-category-position" value=""
                                >
                        </div>
                        <div class="alert alert-danger display-error" id="add-category-position-error">Please Fill in
                            Category Position
                        </div>
                        <!-- End input category position -->

                        <!-- Begin select category parent ID -->
                        <div class="form-group has-error " id="group-category-parent">
                            <label for="category-parent">Select Category Parent</label>
                            <select name="select-add-category-parent" id="select-add-category-parent">
                                class="form-control
                                ">
                                <option value="0">None</option>
                                <?php
                                $query = "SELECT `categoryID`, `categoryName`, `categoryParentID` FROM `category` WHERE 1 ";
                                $result = mysqli_query($link, $query) or die(mysqli_connect_error());
                                while ($row = mysqli_fetch_array($result)) {
                                    echo "<option value=\"$row[0]\">$row[1]</option>";
                                }


                                ?>

                            </select>
                        </div>

                        <!-- End select category parent ID -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" style="margin-top:8px" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" id="submit" name="submit" value="submit" class="btn btn-success"
                                style="margin-top: 10px">Save
                        </button>
                    </div>
            </form>
        </div>
    </div>
    <!-- end add category modal -->


    <script>
        setTimeout(function () {
            // Do something after 3 seconds
            $('#div-add-success').fadeOut();
        }, 3000);

        function validateEditForm() {
            var isValid = true;
            var cat_name = document.forms['form-edit-category']['input-edit-category-name'].value;
            if (cat_name == null || cat_name == "") {
                $('#group-edit-category-name').addClass('has-error');
                $('#edit-category-name-error').fadeIn();

                isValid = false;
            }
            var cat_pos = document.forms['form-edit-category']['input-edit-category-position'].value;
            if (cat_pos == null || cat_pos == "") {
                $('#group-edit-category-position').addClass('has-error');
                $('#edit-category-position-error').fadeIn();

                isValid = false;
            }
            return isValid;
        }

        function validateForm() {
            var isValid = true;
            var cat_name = document.forms['form-add-category']['input-add-category-name'].value;
            if (cat_name == null || cat_name == "") {
                $('#group-add-category-name').addClass('has-error');
                $('#add-category-name-error').slideDown();

                isValid = false;
            }
            var cat_pos = document.forms['form-add-category']['input-add-category-position'].value;
            if (cat_pos == null || cat_pos == "") {
                $('#group-add-category-position').addClass('has-error');
                $('#add-category-position-error').slideDown();

                isValid = false;
            }
            return isValid;
        }

        //auto focus when open a modal
        $('#add-category-modal').on('shown.bs.modal', function () {
            $('#input-add-category-name').focus();
        })

        //auto focus when open a modal
        $('#edit-category-modal').on('shown.bs.modal', function () {
            $('#input-edit-category-name').focus();
        })

        function removeError(typeOfModal, typeOfInput) {
            var name = '#group-' + typeOfModal + '-category-' + typeOfInput
            var position = '#' + typeOfModal + '-category-' + typeOfInput + '-error'
            $(name).removeClass('has-error');
            $(position).slideUp();
        }

        $('#input-edit-category-name').focus(function () {
            removeError('edit', 'name')
        });
        $('#input-edit-category-position').focus(function () {
            removeError('edit', 'position')
        });

        $('#input-add-category-name').focus(function () {
            removeError('add', 'name')
        });
        $('#input-add-category-position').focus(function () {
            removeError('add', 'position')
        });

        $('#edit-category-modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var catid = button.data('catid')
            var catname = button.data('catname')
            var catparent = button.data('catparent')
            var catposition = button.data('catposition')// Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#edit-modal-title').text('Edit Category ' + catname)
            modal.find('#input-edit-category-id').val(catid)
            modal.find('#input-edit-category-name').val(catname)
            modal.find('#input-edit-category-position').val(catposition)
            modal.find('#select-edit-category-parent').val(catparent)

        })
    </script>
<?php include_once 'include/footer.php'; ?>